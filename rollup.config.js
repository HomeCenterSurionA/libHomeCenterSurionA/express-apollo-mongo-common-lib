import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import { uglify } from 'rollup-plugin-uglify';

export default {
  input: 'src/index.js',
  output: {
    file: 'lib/index.js',
    format: 'cjs',
    exports: 'named',
  },
  external: [
    'http',
    'apollo-server-express',
    'app-root-path',
    'body-parser',
    'chalk',
    'compression',
    'cors',
    'express',
    'express-content-length-validator',
    'graphql',
    'helmet',
    'lodash',
    'morgan',
    'winston',
  ],
  plugins: [
    commonjs(),
    resolve(),
    babel({
      exclude: 'node_modules/**',
      babelrc: false,
      runtimeHelpers: true,
      presets: ['@babel/preset-env'],
      plugins: ['@babel/plugin-proposal-class-properties', '@babel/plugin-transform-runtime'],
    }),
    uglify(),
  ],
};

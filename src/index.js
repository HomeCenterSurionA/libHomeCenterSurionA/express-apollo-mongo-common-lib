import http from 'http';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import contentLength from 'express-content-length-validator';
import compression from 'compression';
import helmet from 'helmet';
import { ApolloServer, gql } from 'apollo-server-express';

import { Logger, requestLogger } from './logger';
import CONSTANTS from './constants';

const logger = Logger(CONSTANTS.loggerLabel);

export const timeOneServerGraphQL = (appTypeDefs = '', appQueryDefs = '', appResolvers = {}) => {
  const app = express();

  app.server = http.createServer(app);
  app.use(cors());
  app.use(helmet());
  if (process.env.NODE_ENV !== 'test') {
    app.use(
      morgan(
        (tokens, req, res) =>
          Buffer.from(
            JSON.stringify({
              method: tokens.method(req, res),
              url: tokens.url(req, res),
              status: tokens.status(req, res),
              'content-length': tokens.res(req, res, 'content-length'),
              'response-time': `${tokens['response-time'](req, res)}ms`,
              'remote-addr': tokens['remote-addr'](req, res),
              'user-agent': tokens['user-agent'](req, res),
            })
          ),
        { stream: requestLogger.stream }
      )
    );
  }
  app.use(bodyParser.json());
  app.use(compression());
  app.use(contentLength.validateMax({ max: 9999 }));
  app.get('/', (req, res) => {
    res.json({ foo: 'bar' });
  });

  const typeDefs = gql`
    ${appTypeDefs}
    type Query {
      ${appQueryDefs}
    }
  `;

  const resolvers = {
    Query: {
      ...appResolvers,
    },
  };

  const server = new ApolloServer({
    typeDefs,
    resolvers,
    gui: {
      endpoint: 'graphql',
    },
    tracing: process.env.NODE_ENV !== 'production',
    playground: process.env.NODE_ENV !== 'production',
    cacheControl: {
      defaultMaxAge: 120,
    },
  });

  server.applyMiddleware({ app });

  const port = process.env.PORT || 5000;

  return app.listen(
    { port },
    () =>
      process.env.NODE_ENV === 'development' &&
      logger.info(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`)
  );
};

export { timeOneServerGraphQL as default, Logger };

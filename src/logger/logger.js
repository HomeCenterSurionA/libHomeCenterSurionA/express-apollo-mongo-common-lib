import { Container, createLogger, format, transports } from 'winston';

import { loggerFormat, requestFormat } from './format';
import CONSTANTS from '../constants';

const { combine, timestamp, label } = format;
const labelLogger = CONSTANTS.loggerLabel;
const defaultOptions = {
  level: process.env.NODE_ENV === 'development' ? 'debug' : 'info',
  handleExceptions: true,
  colorize: false,
};
const options = {
  console: labelName => ({
    ...defaultOptions,
    format: combine(label({ label: labelName }), timestamp(), loggerFormat),
  }),
  request: labelName => ({
    ...defaultOptions,
    format: combine(label({ label: labelName }), timestamp(), requestFormat),
  }),
};

const container = new Container();

container.add(labelLogger, {
  transports: [new transports.Console(options.console(labelLogger))],
  exitOnError: false,
});

export const Logger = name => {
  if (container.has(name)) {
    return container.get(name);
  }

  container.add(name, {
    transports: [new transports.Console(options.console(name))],
    exitOnError: false,
  });

  return container.get(name);
};

const requestLogger = createLogger({
  transports: [new transports.Console(options.request(labelLogger))],
  exitOnError: false,
});

requestLogger.stream = {
  write(message) {
    requestLogger.info(message);
  },
};

export { Logger as default, requestLogger };

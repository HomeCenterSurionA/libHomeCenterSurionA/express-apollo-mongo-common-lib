import chalk from 'chalk';
import { format } from 'winston';

const { printf } = format;

export const loggerFormat = printf(options => {
  let colorized;
  let messages = '';
  const labelFormat = `[${options.label}]`;

  (Array.isArray(options.message) ? options.message : [options.message]).map(message => {
    if (typeof message === 'object') {
      messages += `${message.stack} `;

      return message;
    }
    messages += `${JSON.stringify(message)} `;

    return message;
  });

  switch (options.level) {
    case 'info':
      colorized = chalk.greenBright;
      break;
    case 'debug':
      colorized = chalk.cyanBright;
      messages = chalk.cyan(messages);
      break;
    case 'verbose':
      colorized = chalk.blueBright;
      break;
    case 'warn':
      colorized = chalk.yellowBright;
      messages = chalk.yellow(messages);
      break;
    case 'error':
      colorized = chalk.redBright;
      messages = chalk.red(messages);
      break;
    default:
      colorized = message => message;
      break;
  }

  return `${chalk.yellowBright(options.timestamp)} ${chalk.blueBright(labelFormat)} ${colorized(
    options.level.toUpperCase()
  )}: ${messages}`;
});

export const requestFormat = printf(options => {
  let colorized;
  const labelFormat = `[${options.label}]`;
  const request = JSON.parse(options.message);

  switch (true) {
    case parseInt(request.status, 10) > 399:
      colorized = chalk.redBright;
      break;
    case parseInt(request.status, 10) > 299:
      colorized = chalk.yellowBright;
      break;
    default:
      colorized = chalk.greenBright;
      break;
  }

  return `${chalk.yellowBright(options.timestamp)} ${chalk.blueBright(labelFormat)}: ${colorized(
    request.method
  )} ${colorized(request.status)} - ${colorized(request.url)} in ${colorized(
    request['response-time']
  )} with content-length ${colorized(request['content-length'])} - ${request['remote-addr']} - ${
    request['user-agent']
  }`;
});

export default { loggerFormat, requestFormat };

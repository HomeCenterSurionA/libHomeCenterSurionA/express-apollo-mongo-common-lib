import { timeOneServerGraphQL, Logger } from './index';

const types = `
  type Foo {
    name: String
  }
`;
const queries = `
  getFoo: Foo
  getFoos: [Foo]
  `;

const resolvers = {
  getFoo: () => ({ name: 'Bar' }),
  getFoos: () => [{ name: 'Bar' }, { name: 'BarFOO' }, { name: 'Foo' }],
};

timeOneServerGraphQL(types, queries, resolvers);
const logger = Logger('TEST logging');

logger.info('My app information');
logger.warn('My app warning');
logger.error('My app error');
